import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {isLoggedIn: false};
    }

    toggleUser = ()=>{
        this.setState(previousState => {
            return { isLoggedIn: !previousState.isLoggedIn};
        })
    }

    render(){
        let display = this.state.isLoggedIn ? 'UserName' : this.props.message;
        return(
            <View style={styles.header}>
                <Text 
                    style={styles.text} 
                    onPress={this.toggleUser}>{display}
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text : {
        textAlign: 'center',
        fontSize: 20,
        flex: 1,
        width: '100%',
        height:60,
        color:'#ffffff',
        borderColor: '#ffffff',
        borderWidth: 5
    },
    header: {
        flex:1,
        fontSize: 20
    }

})
