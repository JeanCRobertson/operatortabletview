import React from 'react';
import {StyleSheet, View, Text, ImageBackground} from 'react-native';

export default class Background extends React.Component{
    render(){
        return(
            <ImageBackground
                style={styles.backgroundStyle}
                source={ require('../assets/images/Wallpaper_Icon.jpg')}>

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    backgroundStyle: {
        width: '50%',
        height: '50%'
    }
})