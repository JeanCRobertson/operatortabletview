import axios from "axios";

export default login = async(username, password) => {
  if (username === '') {
    return {
      error: true,
      message: 'Username cannot be empty'
    };
  }

  if (password === '') {
    return {
      error: true,
      message: 'Password cannot be empty'
    };
  }

  try {
    const { data } = await axios.post('https://api.accuminetech.com/v2/users/login', {
      user: {
        username: username,
        password: password,
        factoryId: 'sim'
      }
    });

    if (data.error) {
      return {
        error: true,
        message: data.error
      };
    } else {
      return {
        error: false,
        token: data.user.token
      };
    }
  } catch (error) {
    let response = {
      error: true,
      message: ''
    };

    if (!error.response || error.response.status === 400) {
      response.message = 'An unexpected error has occured, please try again later';
    } else if (error.response.status === 502 || error.response.status === 404) {
      response.message = 'Accumine Cloud is not available';
    }
    return response;
  }
}