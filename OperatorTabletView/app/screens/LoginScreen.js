import React, { Fragment } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Button,
  AsyncStorage,
  Image
} from 'react-native'
import login from '../Controllers/LoginController';

const Logo = require('../assets/images/logo.jpg');

export default class Login extends React.Component {

    //default navigation creates a header
    static navigationOptions = {
        header: null
    };

    state = {
        username: '', password: '', attempts:0
    }
    onChangeText = (key, value) => {
        this.setState({ [key]: value })
    }
    signIn = async () => {

        const { error, message, token } = await login(this.state.username,this.state.password);

        if (error) {
          alert(message);
        } else {
          await AsyncStorage.setItem('userToken', token);
          this.props.navigation.navigate('Home');
        }
    };

    render() {
        return (
        <View style={styles.container}>

            <Image style={styles.Logo} source={Logo}></Image>
            <TextInput
            style={styles.input}
            placeholder='Username'
            autoCapitalize="none"
            autoCorrect={false}
            placeholderTextColor='white'
            onChangeText={val => this.onChangeText('username', val)}
            />
            <TextInput
            style={styles.input}
            placeholder='Password'
            autoCapitalize="none"
            secureTextEntry={true}
            placeholderTextColor='white'
            onChangeText={val => this.onChangeText('password', val)}
            />
            <Button
            title='Login'
            onPress={this.signIn}
            />
        </View>
        )
    }
    }

    const styles = StyleSheet.create({
    input: {
        width: 350,
        fontSize: 18,
        fontWeight: '500',
        height: 55,
        backgroundColor: '#42A5F5',
        margin: 10,
        color: 'white',
        padding: 8,
        borderRadius: 14
    },
    Logo:{
        width:100,
        height:100,


    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
    })