import React from 'react';
import {StyleSheet, View} from 'react-native';
import LoginScreen from './app/screens/LoginScreen';
import HomeScreen from './app/screens/HomeScreen';
import {createStackNavigator, createAppContainer} from 'react-navigation';

const Routes = createAppContainer(createStackNavigator({
  Login: {
    screen: LoginScreen
  },
  Home:{
    screen: HomeScreen
  }
},
  {
    initialRouteName: 'Login'
  }));

export default class App extends React.Component{
  render(){
    return(
      <Routes/>
    )
  }
}

